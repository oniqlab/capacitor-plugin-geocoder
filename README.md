# capacitor-plugin-geocoder
Capacitory plugin for Reverse and forward geocode. Note: This plugin is not tested high enought, and it's
still in experimental phase, use at your own risk.

## Demo
Soon.

## Maintainers

| Maintainer | Gitlab | Contact |
| --- | --- | --- |
| Fernando Zamperin  | [OniQ Tecnologia](https://gitlab.com/oniqlab)  | [ Website](https://oniq.com.br)

## Installation

```bash
$ npm i --save @oniq/capacitor-plugin-geocoder
```

Using yarn?

```bash
yarn add @oniq/capacitor-plugin-geocoder
```

## Android configuration

In file `android/app/src/main/java/**/**/MainActivity.java`, add the plugin to the initialization list:

```diff
  import br.com.oniq.geocoder.NativeGeocoder;

  this.init(savedInstanceState, new ArrayList<Class<? extends Plugin>>() {{
+   add(NativeGeocoder.class); //Add class here
  }});
```

## iOS configuration

No extra step is needed

## API

### Reverse Geocode
Transforms latitude and longitude into a Result object containing information about a place (can be a street or a known placemark)

```ts
import { Plugins } from '@capacitor/core';
const { NativeGeocoder } = Plugins;

NativeGeocoder.reverseGeocode({
    latitude: -23.6065943,
    longitude: -46.7468018,
}).then(result => {
    // Result is an array containing adresses information;
}).catch(err => {
    console.error(err)
})
```

### Parameters
An object containing the following parameters:

| Parameter | Type | Optional | Description |
| ---------------- | ---------- | ------- | ------------------------------------------------------------- |
| `latitude` | `Number` | No | Latitude |
| `longitude` | `Number` | No | Longitude |
| `options` | `Object` | Yes | Object containing optional options |

All available `options` attributes:

| Attribute | Type | Comment |
| ------------------------------ | ------------------------------------------------------------ | -------------------------------------------------- |
| `locale`  | `String` | Optional. Force the locale for the results, if not set will be used the devices default  |
| `maxResults` | `Number` | Optional. Min and default value: 1, max value: 5 |

### Forward Geocode
Transforms a string of an address into a full complete address object containing Latitude and Longitude

```ts
import { Plugins } from '@capacitor/core';
const { NativeGeocoder } = Plugins;

NativeGeocoder.forwardGeocode({
    address: "San Francisco" //Can be a city, street, or some known place;\
}).then(result => {
    // Result is an array containing adresses information;
}).catch(err => {
    console.error(err)
})
```

### Parameters
An object containing the following parameters:

| Parameter | Type | Optional | Description |
| ---------------- | ---------- | ------- | ------------------------------------------------------------- |
| `address` | `String` | No | The string to be geocoded (can be street, avenue, known place) |
| `options`   | `Object` | Yes | Object containing optional options |

All available `options` attributes:

| Attribute | Type | Comment |
| ------------------------------ | ------------------------------------------------------------ | -------------------------------------------------- |
| `locale`  | `String` | Optional. Force the locale for the results, if not set will be used the devices default  |
| `maxResults` | `Number` | Optional. Min and default value: 1, max value: 5 |