declare module '@capacitor/core' {
  interface PluginRegistry {
    NativeGeocoder: NativeGeocoderPlugin;
  }
}

export interface NativeGeocoderPlugin {
  /**
   * Get Latitude and Longitude and converts it into a address information
   * @returns  {Promise<Result>}
   * @rejects string
   */
  reverseGeocode(reverseGeocodeOptions: ReverseGeocodeOptions): Promise<Result>;
  /**
   * Gets address information and converts it into a Latitude and Longitude
   * @returns  {Promise<Result>}
   * @rejects string
   */
  forwardGeocode(forwardGeocodeOptions: ForwardGeocodeOptions): Promise<Result>;
}

export interface ReverseGeocodeOptions {
  latitude: number;
  longitude: number;
  options?: Options;
}

export interface ForwardGeocodeOptions {
  address: string;
  options?: Options;
}

export interface Options {
  locale?: string;
  maxResults?: number;
}

export interface Result {
  latitude: number;
  longitude: number;
  countryCode: string;
  postalCode: string;
  administrativeArea: string;
  subAdministrativeArea: string;
  locality: string;
  subLocality: string;
  thoroughfare: string;
  subThoroughfare: string;
  areasOfInterest: string[];
}
