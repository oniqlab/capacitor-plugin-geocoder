import { WebPlugin } from '@capacitor/core';
import {
  NativeGeocoderPlugin, Result, ReverseGeocodeOptions, ForwardGeocodeOptions
} from './definitions';

export class NativeGeocoderWeb extends WebPlugin implements NativeGeocoderPlugin {
  constructor() {
    super({
      name: 'NativeGeocoder',
      platforms: ['web'],
    });
  }

  // @ts-ignore
  reverseGeocode(reverseGeocodeOptions: ReverseGeocodeOptions): Promise<Result> {
    throw new Error("Method not implemented.");
  }

  // @ts-ignore
  forwardGeocode(forwardGeocodeOptions: ForwardGeocodeOptions): Promise<Result> {
    throw new Error("Method not implemented.");
  }
}

const NativeGeocoder = new NativeGeocoderWeb();

export { NativeGeocoder };

import { registerWebPlugin } from '@capacitor/core';
registerWebPlugin(NativeGeocoder);