package br.com.oniq.geocoder;

import java.util.Locale;

public class GeocodeOptions {
    private Locale locale = Locale.getDefault();
    private Integer maxResults = 1;

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale defaultLocale) {
        this.locale = defaultLocale;
    }

    public Integer getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }
}
